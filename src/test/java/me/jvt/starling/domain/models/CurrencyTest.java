package me.jvt.starling.domain.models;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class CurrencyTest {
  @Nested
  class From {
    @Test
    void usesName() {
      Currency actual = Currency.from("GBP");

      assertThat(actual).isEqualTo(Currency.POUND_STERLING);
    }

    @ParameterizedTest
    @EnumSource(value = Currency.class)
    void allValuesHaveMappings(Currency currency) {
      Currency actual = Currency.from(currency.getIsoName());

      assertThat(actual).isNotNull();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void whenNotValid(String currency) {
      assertThatThrownBy(() -> Currency.from(currency))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Could not find a valid Currency from the provided value `" + currency + "`");
    }
  }
}
