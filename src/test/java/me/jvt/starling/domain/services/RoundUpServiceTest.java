package me.jvt.starling.domain.services;

import static me.jvt.starling.support.CurrencyAndAmountAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Set;
import me.jvt.starling.domain.models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class RoundUpServiceTest {

  private final RoundUpService service = new RoundUpService();

  @Nested
  class RoundUp {
    private me.jvt.starling.domain.models.RoundUp roundUp;

    @Nested
    class WhenNothingToRound {

      @BeforeEach
      void setup() {
        var toRound =
            new Transaction("", new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(0)));
        roundUp = service.roundUp(toRound);
      }

      @Test
      void hasZeroPenniesToRoundUp() {
        assertThat(roundUp.roundUp()).hasMinorAmounts(0);
      }

      @Test
      void hasPoundSterlingCurrency() {
        assertThat(roundUp.roundUp()).hasCurrency(Currency.POUND_STERLING);
      }
    }

    @Nested
    class WhenFiftyPennies {

      @BeforeEach
      void setup() {
        var toRound =
            new Transaction("", new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(50)));
        roundUp = service.roundUp(toRound);
      }

      @Test
      void hasFiftyPenniesToRoundUp() {
        assertThat(roundUp.roundUp()).hasMinorAmounts(50);
      }

      @Test
      void hasPoundSterlingCurrency() {
        assertThat(roundUp.roundUp()).hasCurrency(Currency.POUND_STERLING);
      }
    }

    @Nested
    class WhenOnePennyToRoundUp {

      @BeforeEach
      void setup() {
        var toRound =
            new Transaction("", new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(99)));
        roundUp = service.roundUp(toRound);
      }

      @Test
      void hasOnePennyToRoundUp() {
        assertThat(roundUp.roundUp()).hasMinorAmounts(1);
      }

      @Test
      void hasPoundSterlingCurrency() {
        assertThat(roundUp.roundUp()).hasCurrency(Currency.POUND_STERLING);
      }
    }

    @Nested
    class WhenMoreThanOneHundred {

      @BeforeEach
      void setup() {
        var toRound =
            new Transaction("", new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(101)));
        roundUp = service.roundUp(toRound);
      }

      @Test
      void hasNinetyNinePenniesToRoundUp() {
        assertThat(roundUp.roundUp()).hasMinorAmounts(99);
      }

      @Test
      void hasPoundSterlingCurrency() {
        assertThat(roundUp.roundUp()).hasCurrency(Currency.POUND_STERLING);
      }
    }

    @Nested
    class OnlySupportsPoundSterling {
      @Test
      void poundSterlingIsSupported() {
        var toRound =
            new Transaction("", new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(99)));
        assertThatCode(() -> service.roundUp(toRound)).doesNotThrowAnyException();
      }

      @ParameterizedTest
      @EnumSource(
          names = {"POUND_STERLING"},
          value = Currency.class,
          mode = EnumSource.Mode.EXCLUDE)
      void otherCurrenciesAreNotSupported(Currency currency) {
        var toRound = new Transaction("", new CurrencyAndAmount(currency, Amount.of(99)));
        assertThatThrownBy(() -> service.roundUp(toRound))
            .isInstanceOf(RoundUpService.CurrencyNotSupportedException.class);
      }
    }
  }

  @Nested
  class CalculateTotalRoundUp {
    private final CurrencyAndAmount amount =
        new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(99));

    private final Transaction transaction = new Transaction("", amount);
    private final me.jvt.starling.domain.models.RoundUp rounded =
        new me.jvt.starling.domain.models.RoundUp(transaction, amount);

    @Test
    void whenValidItSumsMinorAmounts() {
      var toRound =
          Set.of(
              new me.jvt.starling.domain.models.RoundUp(new Transaction("1", null), amount),
              new me.jvt.starling.domain.models.RoundUp(new Transaction("2", null), amount),
              new me.jvt.starling.domain.models.RoundUp(new Transaction("3", null), amount));

      CurrencyAndAmount actual = service.calculateTotalRoundUp(toRound);

      assertThat(actual).hasCurrency(Currency.POUND_STERLING).hasMinorAmounts(3 * 99);
    }

    @Nested
    class OnlySupportsPoundSterling {
      @Test
      void poundSterlingIsSupported() {
        assertThatCode(() -> service.calculateTotalRoundUp(Set.of(rounded)))
            .doesNotThrowAnyException();
      }

      @ParameterizedTest
      @EnumSource(
          names = {"POUND_STERLING"},
          value = Currency.class,
          mode = EnumSource.Mode.EXCLUDE)
      void otherCurrenciesAreNotSupported(Currency currency) {
        var amount = new CurrencyAndAmount(currency, Amount.of(99));
        var rounded = new me.jvt.starling.domain.models.RoundUp(null, amount);
        assertThatThrownBy(() -> service.calculateTotalRoundUp(Set.of(rounded)))
            .isInstanceOf(RoundUpService.CurrencyNotSupportedException.class);
      }
    }
  }
}
