package me.jvt.starling.application;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
class ApplicationIntegrationTest {
  @Test
  void contextLoads(ApplicationContext applicationContext) {
    assertThat(applicationContext).isNotNull();
  }
}
