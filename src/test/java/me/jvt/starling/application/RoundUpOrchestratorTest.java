package me.jvt.starling.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import me.jvt.starling.application.models.WeekOf;
import me.jvt.starling.application.repositories.RoundUpRepository;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.domain.services.RoundUpService;
import me.jvt.starling.domain.services.SavingsGoalService;
import me.jvt.starling.domain.services.TransactionsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoundUpOrchestratorTest {

  @Mock private TransactionsService transactionsService;
  @Mock private RoundUpRepository roundUpRepository;
  @Mock private RoundUpService roundUpService;
  @Mock private SavingsGoalService savingsGoalService;

  @InjectMocks private RoundUpOrchestrator orchestrator;

  @Nested
  class WeekOfTest {

    @Nested
    class CalculatesNormalWeek {
      @Test
      void start() {
        Instant now = Instant.parse("2022-02-09T08:40:45+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.start()).isEqualTo("2022-02-07T00:00:00+00:00");
      }

      @Test
      void end() {
        Instant now = Instant.parse("2022-02-09T08:40:45+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.end()).isEqualTo("2022-02-13T23:59:59+00:00");
      }
    }

    @Nested
    class calculatesOverNewYearsDay {
      @Test
      void start() {
        Instant now = Instant.parse("2021-12-31T08:40:45+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.start()).isEqualTo("2021-12-27T00:00:00+00:00");
      }

      @Test
      void end() {
        Instant now = Instant.parse("2021-12-31T08:40:45+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.end()).isEqualTo("2022-01-02T23:59:59+00:00");
      }
    }

    @Nested
    class WhenMonday {

      @Test
      void start() {
        Instant now = Instant.parse("2022-02-07T09:00:00+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.start()).isEqualTo("2022-02-07T00:00:00+00:00");
      }

      @Test
      void end() {
        Instant now = Instant.parse("2022-02-07T09:00:00+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.end()).isEqualTo("2022-02-13T23:59:59+00:00");
      }
    }

    @Nested
    class WhenSunday {

      @Test
      void start() {
        Instant now = Instant.parse("2022-02-13T23:00:00+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.start()).isEqualTo("2022-02-07T00:00:00+00:00");
      }

      @Test
      void end() {
        Instant now = Instant.parse("2022-02-07T09:00:00+00:00");

        WeekOf weekOf = orchestrator.weekOf(now);

        assertThat(weekOf.end()).isEqualTo("2022-02-13T23:59:59+00:00");
      }
    }
  }

  @Nested
  class PerformRoundUpForWeek {

    private final Instant start = Instant.parse("2022-02-07T00:00:00+00:00");
    private final Instant end = Instant.parse("2022-02-13T23:59:59+00:00");
    private final WeekOf weekOf = new WeekOf(start, end);
    private final CurrencyAndAmount amount =
        new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(1));

    @Captor private ArgumentCaptor<Transfer> transferArgumentCaptor;

    @Nested
    class WhenTransferRequired {

      @BeforeEach
      void setup() {
        when(roundUpService.calculateTotalRoundUp(any())).thenReturn(amount);
        when(savingsGoalService.transferRoundUp(any(), any(), any(), any()))
            .thenReturn(new Transfer(UUID.randomUUID()));
      }

      @Test
      void retrievesTransactionsFromTransactionsService() {
        UUID accountUid = UUID.randomUUID();
        orchestrator.performRoundUpForWeek(weekOf, accountUid, null, null, "j.w.t");

        verify(transactionsService).findForPeriod(new Account(accountUid), start, end, "j.w.t");
      }

      @Test
      void retrievesRoundUpsFromRoundUpRepository() {
        Set<Transaction> transactions =
            Set.of(new Transaction("1", amount), new Transaction("2", amount));
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);

        orchestrator.performRoundUpForWeek(weekOf, null, null, null, null);

        verify(roundUpRepository).findById("1");
        verify(roundUpRepository).findById("2");
      }

      @Test
      void createsRoundUpForAbsentIds() {
        Transaction toRound = new Transaction("2", amount);
        Set<Transaction> transactions = Set.of(new Transaction("1", amount), toRound);
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        when(roundUpRepository.findById(any())).thenReturn(Optional.empty());
        when(roundUpRepository.findById("1"))
            .thenReturn(Optional.of(new RoundUp(new Transaction("1", amount), amount)));

        orchestrator.performRoundUpForWeek(weekOf, null, null, null, null);

        verify(roundUpService).roundUp(toRound);
      }

      @Test
      void delegatesSavingsGoalToSavingsGoalService() {
        Transaction ignored = new Transaction("1", amount);
        Set<Transaction> transactions =
            Set.of(
                ignored,
                new Transaction("2", amount),
                new Transaction("3", amount),
                new Transaction("4", amount));
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        when(roundUpRepository.findById(any())).thenReturn(Optional.empty());
        when(roundUpRepository.findById("1")).thenReturn(Optional.of(new RoundUp(ignored, amount)));

        UUID accountUid = UUID.randomUUID();
        UUID savingsGoalUid = UUID.randomUUID();
        UUID transferUid = UUID.randomUUID();

        Account expectedAccount = new Account(accountUid);
        SavingsGoal expectedSavingsGoal = new SavingsGoal(expectedAccount, savingsGoalUid, null);

        orchestrator.performRoundUpForWeek(
            weekOf, accountUid, savingsGoalUid, transferUid, "j.w.t");

        verify(savingsGoalService).transferRoundUp(eq(expectedSavingsGoal), any(), any(), any());
      }

      @Test
      void delegatesCalculatedRoundUpToSavingsGoalService() {
        Transaction ignored = new Transaction("1", amount);
        Set<Transaction> transactions =
            Set.of(
                ignored,
                new Transaction("2", amount),
                new Transaction("3", amount),
                new Transaction("4", amount));
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        when(roundUpRepository.findById(any())).thenReturn(Optional.empty());
        when(roundUpRepository.findById("1")).thenReturn(Optional.of(new RoundUp(ignored, amount)));

        UUID accountUid = UUID.randomUUID();
        UUID savingsGoalUid = UUID.randomUUID();
        UUID transferUid = UUID.randomUUID();

        CurrencyAndAmount expectedRoundUp =
            new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(12345));
        when(roundUpService.calculateTotalRoundUp(any())).thenReturn(expectedRoundUp);

        orchestrator.performRoundUpForWeek(
            weekOf, accountUid, savingsGoalUid, transferUid, "j.w.t");

        verify(savingsGoalService).transferRoundUp(any(), eq(expectedRoundUp), any(), any());
      }

      @Test
      void delegatesTransferToSavingsGoalService() {
        Transaction ignored = new Transaction("1", amount);
        Set<Transaction> transactions =
            Set.of(
                ignored,
                new Transaction("2", amount),
                new Transaction("3", amount),
                new Transaction("4", amount));
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        when(roundUpRepository.findById(any())).thenReturn(Optional.empty());
        when(roundUpRepository.findById("1")).thenReturn(Optional.of(new RoundUp(ignored, amount)));

        UUID accountUid = UUID.randomUUID();
        UUID savingsGoalUid = UUID.randomUUID();
        UUID transferUid = UUID.randomUUID();

        Transfer expected = new Transfer(transferUid);

        orchestrator.performRoundUpForWeek(
            weekOf, accountUid, savingsGoalUid, transferUid, "j.w.t");

        verify(savingsGoalService).transferRoundUp(any(), any(), eq(expected), any());
      }

      @Test
      void delegatesBearerTokenToSavingsGoalService() {
        Transaction ignored = new Transaction("1", amount);
        Set<Transaction> transactions =
            Set.of(
                ignored,
                new Transaction("2", amount),
                new Transaction("3", amount),
                new Transaction("4", amount));
        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        when(roundUpRepository.findById(any())).thenReturn(Optional.empty());
        when(roundUpRepository.findById("1")).thenReturn(Optional.of(new RoundUp(ignored, amount)));

        UUID accountUid = UUID.randomUUID();
        UUID savingsGoalUid = UUID.randomUUID();
        UUID transferUid = UUID.randomUUID();

        orchestrator.performRoundUpForWeek(
            weekOf, accountUid, savingsGoalUid, transferUid, "j.w.t");

        verify(savingsGoalService).transferRoundUp(any(), any(), any(), eq("j.w.t"));
      }

      @Test
      void returnsTransferFromSavingsGoalService() {
        Transfer expected = new Transfer(UUID.randomUUID());
        when(savingsGoalService.transferRoundUp(any(), any(), any(), any())).thenReturn(expected);

        Optional<Transfer> actual =
            orchestrator.performRoundUpForWeek(
                weekOf, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "j.w.t");

        assertThat(actual).isPresent().contains(expected);
      }

      @Test
      void savesTransactionsInRoundUpRepository() {
        Transaction transaction0 = new Transaction("0", amount);
        setUpRoundUpMock(transaction0);

        Set<Transaction> transactions = Set.of(transaction0);

        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        Transfer expected = new Transfer(UUID.randomUUID());
        when(savingsGoalService.transferRoundUp(any(), any(), any(), any())).thenReturn(expected);

        orchestrator.performRoundUpForWeek(
            weekOf, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), null);

        InOrder inOrder = Mockito.inOrder(savingsGoalService, roundUpRepository);

        inOrder.verify(savingsGoalService).transferRoundUp(any(), any(), any(), any());
        inOrder.verify(roundUpRepository).save(any());
      }

      @Test
      void savesTransactionsInRoundUpRepositoryAfterTransferringRoundUp() {
        Transaction transaction0 = new Transaction("0", amount);
        Transaction transaction1 = new Transaction("1", amount);
        Transaction transaction2 = new Transaction("2", amount);
        RoundUp roundUp0 = setUpRoundUpMock(transaction0);
        RoundUp roundUp1 = setUpRoundUpMock(transaction1);
        RoundUp roundUp2 = setUpRoundUpMock(transaction2);

        Set<Transaction> transactions = Set.of(transaction0, transaction1, transaction2);

        when(transactionsService.findForPeriod(any(), any(), any(), any()))
            .thenReturn(transactions);
        Transfer expected = new Transfer(UUID.randomUUID());
        when(savingsGoalService.transferRoundUp(any(), any(), any(), any())).thenReturn(expected);

        orchestrator.performRoundUpForWeek(
            weekOf, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), null);

        verify(roundUpRepository).save(roundUp0);
        verify(roundUpRepository).save(roundUp1);
        verify(roundUpRepository).save(roundUp2);
      }
    }

    @Nested
    class WhenNoTransfer {

      @Test
      void returnsEmptyOptionalIfTransferTotalsZero() {
        when(roundUpService.calculateTotalRoundUp(any()))
            .thenReturn(new CurrencyAndAmount(null, Amount.of(0)));

        Optional<Transfer> actual =
            orchestrator.performRoundUpForWeek(
                weekOf, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "j.w.t");

        assertThat(actual).isEmpty();
      }
    }

    private RoundUp setUpRoundUpMock(Transaction transaction) {
      RoundUp roundUp = new RoundUp(transaction, amount);
      when(roundUpService.roundUp(transaction)).thenReturn(roundUp);
      return roundUp;
    }

    @Disabled(
        "We would ideally have set up error handling, but I've decided to perform just happy-path for now")
    @Test
    void throwsExceptionWhenTransferFails() {
      fail("TODO");
    }
  }
}
