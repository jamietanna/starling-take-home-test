package me.jvt.starling.interfaces.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.starlingbank.api.models.GetSettledTransactionsBetweenResponse;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.interfaces.exceptions.InternalErrorException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(SpringExtension.class)
@Import({HttpTransactionsServiceIntegrationTest.Config.class, JacksonAutoConfiguration.class})
class HttpTransactionsServiceIntegrationTest {
  @TestConfiguration
  static class Config {

    @Bean
    public MockWebServer webServer() {
      return new MockWebServer();
    }

    @Bean
    public WebClient webClient(MockWebServer webServer) {
      return WebClient.builder().baseUrl(webServer.url("").toString()).build();
    }

    @Bean
    public HttpTransactionsService service(WebClient webClient) {
      return new HttpTransactionsService(webClient);
    }
  }

  @IntegrationTest
  class FindForPeriod {

    @Autowired private ObjectMapper mapper;
    @Autowired private MockWebServer server;

    @Autowired private HttpTransactionsService service;

    @Nested
    class WhenSuccessful {
      private final Instant start =
          Instant.now().minus(14, ChronoUnit.DAYS).truncatedTo(ChronoUnit.SECONDS);
      private final Instant end =
          Instant.now().plus(14, ChronoUnit.DAYS).truncatedTo(ChronoUnit.SECONDS);

      @Test
      void itReturnsTransactions() {
        server.enqueue(
            new MockResponse()
                .setResponseCode(200)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(successBody()));

        CurrencyAndAmount amount = new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(123));

        Set<Transaction> transactions =
            service.findForPeriod(new Account(UUID.randomUUID()), start, end, null);

        assertThat(transactions)
            .contains(new Transaction("1", amount), new Transaction("Another", amount));
      }
    }

    @Nested
    class Request {
      private final UUID accountUid = UUID.randomUUID();
      private final Account account = new Account(accountUid);
      private final Instant start = Instant.parse("2022-02-09T08:40:45+00:00");
      private final Instant end = Instant.parse("2022-02-09T08:40:45+00:00");
      private RecordedRequest request;

      @BeforeEach
      void setup() throws InterruptedException {
        server.enqueue(
            new MockResponse()
                .setResponseCode(200)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(successBody()));

        service.findForPeriod(account, start, end, "eyJ...");

        request = server.takeRequest(1, TimeUnit.SECONDS);
        assertThat(request).isNotNull();
      }

      @Test
      void isGet() {
        assertThat(request.getMethod()).isEqualTo("GET");
      }

      @Test
      void isToCorrectUrl() {
        assertThat(request.getPath())
            .isEqualTo(
                "/api/v2/feed/account/"
                    + accountUid
                    + "/settled-transactions-between?minTransactionTimestamp=2022-02-09T08%3A40%3A45Z&maxTransactionTimestamp=2022-02-09T08%3A40%3A45Z");
      }

      @Test
      void expectsJson() {
        assertThat(request.getHeader("accept")).isEqualTo("application/json");
      }

      @Test
      void itSendsAuthorizationHeader() {
        assertThat(request.getHeader("Authorization")).isEqualTo("Bearer eyJ...");
      }
    }

    @Nested
    class WhenUnsuccessful {
      private final Account account = new Account(UUID.randomUUID());

      @ParameterizedTest
      @ValueSource(ints = {400, 500})
      void throwsInternalErrorException(int status) {
        server.enqueue(
            new MockResponse()
                .setResponseCode(status)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        assertThatThrownBy(() -> service.findForPeriod(account, Instant.now(), Instant.now(), null))
            .isInstanceOf(InternalErrorException.class);
      }
    }

    private String successBody() {
      var transaction0 = new GetSettledTransactionsBetweenResponse.SettledTransaction();
      transaction0.setFeedItemUid("1");

      var transaction1 = new GetSettledTransactionsBetweenResponse.SettledTransaction();
      transaction1.setFeedItemUid("Another");

      var amount = new GetSettledTransactionsBetweenResponse.SettledTransaction.Amount();
      amount.setCurrency("GBP");
      amount.setMinorUnits(123);

      transaction0.setAmount(amount);
      transaction1.setAmount(amount);

      var response = new GetSettledTransactionsBetweenResponse();
      response.setFeedItems(List.of(transaction0, transaction1));

      try {
        return mapper.writeValueAsString(response);
      } catch (JsonProcessingException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  @ExtendWith(SpringExtension.class)
  @Import({HttpTransactionsServiceIntegrationTest.Config.class, JacksonAutoConfiguration.class})
  @DirtiesContext
  @Nested
  @Retention(RetentionPolicy.RUNTIME)
  private @interface IntegrationTest {}
}
