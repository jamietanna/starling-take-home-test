package me.jvt.starling.interfaces.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.starlingbank.api.models.CreateSavingsGoalRequest;
import com.starlingbank.api.models.CreateSavingsGoalResponse;
import com.starlingbank.api.models.TransferToSavingsGoalRequest;
import com.starlingbank.api.models.TransferToSavingsGoalResponse;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.interfaces.exceptions.InternalErrorException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;

class HttpSavingsGoalServiceIntegrationTest {
  @TestConfiguration
  static class Config {

    @Bean
    public MockWebServer webServer() {
      return new MockWebServer();
    }

    @Bean
    public WebClient webClient(MockWebServer webServer) {
      return WebClient.builder().baseUrl(webServer.url("").toString()).build();
    }

    @Bean
    public HttpSavingsGoalService service(WebClient webClient) {
      return new HttpSavingsGoalService(webClient);
    }
  }

  @IntegrationTest
  class CreateRoundUpGoal {

    @Autowired private ObjectMapper mapper;
    @Autowired private MockWebServer server;

    @Autowired private HttpSavingsGoalService service;

    @Nested
    class WhenSuccessful {
      private final Account account = new Account(UUID.randomUUID());
      private final CurrencyAndAmount target =
          new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(123));

      @Test
      void itReturnsUidFromServer() {
        UUID uid = UUID.randomUUID();
        setupSuccess(uid);

        SavingsGoal roundUpGoal = service.createRoundUpGoal(account, null, target, null);

        assertThat(roundUpGoal.uid()).isEqualTo(uid);
      }

      @Test
      void itReturnsAccountFromConsumer() {
        setupSuccess();

        SavingsGoal roundUpGoal = service.createRoundUpGoal(account, null, target, null);

        assertThat(roundUpGoal.account()).isEqualTo(account);
      }

      @Test
      void itReturnsNameFromConsumer() {
        setupSuccess();

        SavingsGoal roundUpGoal = service.createRoundUpGoal(account, "the name", target, null);

        assertThat(roundUpGoal.name()).isEqualTo("the name");
      }
    }

    @Nested
    class WhenUnsuccessful {
      private final Account account = new Account(UUID.randomUUID());
      private final CurrencyAndAmount target =
          new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(123));

      @ParameterizedTest
      @ValueSource(ints = {400, 500})
      void throwsInternalErrorException(int status) {
        server.enqueue(
            new MockResponse()
                .setResponseCode(status)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        assertThatThrownBy(() -> service.createRoundUpGoal(account, "name", target, ""))
            .isInstanceOf(InternalErrorException.class);
      }
    }

    @Nested
    class Request {

      private final UUID accountUid = UUID.randomUUID();
      private final Account account = new Account(accountUid);
      private RecordedRequest request;

      @BeforeEach
      void setup() throws InterruptedException {
        setupSuccess(accountUid);
        CurrencyAndAmount target = new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(1234));
        service.createRoundUpGoal(account, "the name", target, "xyz.123");

        request = server.takeRequest(1, TimeUnit.SECONDS);
        assertThat(request).isNotNull();
      }

      @Test
      void isPut() {
        assertThat(request.getMethod()).isEqualTo("PUT");
      }

      @Test
      void isToCorrectUrl() {
        assertThat(request.getPath()).isEqualTo("/api/v2/account/" + accountUid + "/savings-goals");
      }

      @Test
      void isJsonRequest() {
        assertThat(request.getHeader("Content-Type")).isEqualTo("application/json");
      }

      @Test
      void itSendsAuthorizationHeader() {
        assertThat(request.getHeader("Authorization")).isEqualTo("Bearer xyz.123");
      }

      @Test
      void itSendsBody() throws JsonProcessingException {
        CreateSavingsGoalRequest expected = new CreateSavingsGoalRequest();
        expected.setName("the name");
        expected.setCurrency("GBP");
        CreateSavingsGoalRequest.Target target = new CreateSavingsGoalRequest.Target();
        target.setCurrency("GBP");
        target.setMinorUnits(1234);
        expected.setTarget(target);

        assertThat(request.getBody().readUtf8()).isEqualTo(mapper.writeValueAsString(expected));
      }
    }

    private void setupSuccess() {
      setupSuccess(UUID.randomUUID());
    }

    private void setupSuccess(UUID uid) {
      server.enqueue(
          new MockResponse()
              .setResponseCode(200)
              .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
              .setBody(successBody(uid)));
    }

    private String successBody(UUID uid) {
      CreateSavingsGoalResponse response = new CreateSavingsGoalResponse();
      response.setSavingsGoalUid(uid);
      try {
        return mapper.writeValueAsString(response);
      } catch (JsonProcessingException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  @IntegrationTest
  class TransferRoundUp {

    @Autowired private ObjectMapper mapper;
    @Autowired private MockWebServer server;

    @Autowired private HttpSavingsGoalService service;

    @Nested
    class WhenSuccessful {
      @Test
      void itReturnsTransfer() {
        Account account = new Account(UUID.randomUUID());
        Transfer transfer = new Transfer(UUID.randomUUID());
        SavingsGoal savingsGoal = new SavingsGoal(account, UUID.randomUUID(), null);

        Transfer expected = new Transfer(UUID.randomUUID());
        setupSuccess(expected.transferUid());

        Transfer actual =
            service.transferRoundUp(
                savingsGoal,
                new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(1)),
                transfer,
                null);

        assertThat(actual).isEqualTo(expected);
      }
    }

    @Nested
    class WhenUnsuccessful {
      private final Account account = new Account(UUID.randomUUID());
      private final SavingsGoal savingsGoal = new SavingsGoal(account, UUID.randomUUID(), null);

      @ParameterizedTest
      @ValueSource(ints = {400, 500})
      void throwsInternalErrorException(int status) {
        server.enqueue(
            new MockResponse()
                .setResponseCode(status)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        assertThatThrownBy(
                () ->
                    service.transferRoundUp(
                        savingsGoal,
                        new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(1)),
                        new Transfer(UUID.randomUUID()),
                        null))
            .isInstanceOf(InternalErrorException.class);
      }
    }

    @IntegrationTest
    class Request {

      @Autowired private ObjectMapper mapper;
      @Autowired private MockWebServer server;

      @Autowired private HttpSavingsGoalService service;

      private final UUID accountUid = UUID.randomUUID();
      private final Account account = new Account(accountUid);
      private final UUID transferUid = UUID.randomUUID();
      private final Transfer transfer = new Transfer(transferUid);
      private final UUID savingsGoalUid = UUID.randomUUID();
      private final SavingsGoal savingsGoal = new SavingsGoal(account, savingsGoalUid, null);

      private RecordedRequest request;

      @BeforeEach
      void setup() throws InterruptedException {
        setupSuccess(accountUid);
        CurrencyAndAmount amount = new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(1234));
        service.transferRoundUp(savingsGoal, amount, transfer, "j.w.t");

        request = server.takeRequest(1, TimeUnit.SECONDS);
        assertThat(request).isNotNull();
      }

      @Test
      void isPut() {
        assertThat(request.getMethod()).isEqualTo("PUT");
      }

      @Test
      void isToCorrectUrl() {
        assertThat(request.getPath())
            .isEqualTo(
                "/api/v2/account/"
                    + accountUid
                    + "/savings-goals/"
                    + savingsGoalUid
                    + "/add-money/"
                    + transferUid);
      }

      @Test
      void isJsonRequest() {
        assertThat(request.getHeader("Content-Type")).isEqualTo("application/json");
      }

      @Test
      void itSendsAuthorizationHeader() {
        assertThat(request.getHeader("Authorization")).isEqualTo("Bearer j.w.t");
      }

      @Test
      void itSendsBody() throws JsonProcessingException {
        TransferToSavingsGoalRequest expected = new TransferToSavingsGoalRequest();
        TransferToSavingsGoalRequest.Amount amount = new TransferToSavingsGoalRequest.Amount();
        amount.setCurrency("GBP");
        amount.setMinorUnits(1234);
        expected.setAmount(amount);

        assertThat(request.getBody().readUtf8()).isEqualTo(mapper.writeValueAsString(expected));
      }
    }

    private void setupSuccess() {
      setupSuccess(UUID.randomUUID());
    }

    private void setupSuccess(UUID uid) {
      server.enqueue(
          new MockResponse()
              .setResponseCode(200)
              .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
              .setBody(successBody(uid)));
    }

    private String successBody(UUID uid) {
      TransferToSavingsGoalResponse response = new TransferToSavingsGoalResponse();
      response.setTransferUid(uid.toString());
      try {
        return mapper.writeValueAsString(response);
      } catch (JsonProcessingException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  @ExtendWith(SpringExtension.class)
  @Import({HttpSavingsGoalServiceIntegrationTest.Config.class, JacksonAutoConfiguration.class})
  @DirtiesContext
  @Nested
  @Retention(RetentionPolicy.RUNTIME)
  private @interface IntegrationTest {}
}
