package me.jvt.starling.interfaces.controllers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.UUID;
import me.jvt.starling.domain.models.Account;
import me.jvt.starling.domain.models.SavingsGoal;
import me.jvt.starling.domain.services.SavingsGoalService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@WebMvcTest(SavingsGoalController.class)
@AutoConfigureMockMvc
class SavingsGoalControllerIntegrationTest {
  @MockBean private SavingsGoalService savingsGoalService;

  @Autowired private ObjectMapper objectMapper;
  @Autowired private MockMvc mockMvc;

  @Nested
  class Create {
    private final UUID accountUid = UUID.randomUUID();

    @Test
    void returnsBadRequestWhenNoAuthorizationHeader() throws Exception {
      mockMvc.perform(requestBuilder()).andExpect(status().isBadRequest());
    }

    @ParameterizedTest
    @ValueSource(strings = {"eyJ...", ""})
    void returnsBadRequestWhenInvalidAuthorizationHeader(String authorizationHeader)
        throws Exception {
      mockMvc
          .perform(requestBuilder().header("Authorization", authorizationHeader))
          .andExpect(status().isBadRequest());
    }

    @Test
    void delegatesValueFromBodyToOrchestrator() throws Exception {
      when(savingsGoalService.createRoundUpGoal(any(), any(), any(), any()))
          .thenReturn(new SavingsGoal(null, UUID.randomUUID(), null));

      Account expected = new Account(accountUid);

      mockMvc.perform(requestBuilder().header("Authorization", "Bearer foo"));

      verify(savingsGoalService).createRoundUpGoal(eq(expected), any(), any(), eq("foo"));
    }

    @Test
    void delegatesAuthorizationHeaderToOrchestrator() throws Exception {
      when(savingsGoalService.createRoundUpGoal(any(), any(), any(), any()))
          .thenReturn(new SavingsGoal(null, UUID.randomUUID(), null));

      mockMvc.perform(requestBuilder().header("Authorization", "Bearer foo"));

      verify(savingsGoalService).createRoundUpGoal(any(), any(), any(), eq("foo"));
    }

    @Test
    void returns200Ok() throws Exception {
      when(savingsGoalService.createRoundUpGoal(any(), any(), any(), any()))
          .thenReturn(new SavingsGoal(null, UUID.randomUUID(), null));

      mockMvc
          .perform(requestBuilder().header("Authorization", "Bearer foo"))
          .andExpect(status().isOk());
    }

    private MockHttpServletRequestBuilder requestBuilder() {
      return post("/savings-goals").content(validRequest()).contentType(MediaType.APPLICATION_JSON);
    }

    private String validRequest() {
      ObjectNode body = objectMapper.createObjectNode();
      body.put("accountUid", accountUid.toString());

      try {
        return objectMapper.writeValueAsString(body);
      } catch (JsonProcessingException e) {
        throw new IllegalStateException(e);
      }
    }
  }
}
