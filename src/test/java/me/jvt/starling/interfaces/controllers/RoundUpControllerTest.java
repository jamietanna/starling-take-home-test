package me.jvt.starling.interfaces.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;
import me.jvt.starling.application.RoundUpOrchestrator;
import me.jvt.starling.application.models.WeekOf;
import me.jvt.starling.domain.models.Transfer;
import me.jvt.starling.interfaces.exceptions.BadRequestException;
import me.jvt.starling.interfaces.exceptions.UnprocessableEntity;
import me.jvt.starling.interfaces.models.CreateNewRoundUpRequest;
import me.jvt.starling.interfaces.models.CreateNewRoundUpResponse;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoundUpControllerTest {
  public static final String VALID_AUTH = "Bearer x.y.z";
  @Mock private RoundUpOrchestrator orchestrator;
  @InjectMocks private RoundUpController controller;

  @Nested
  class PerformRoundUp {
    private final CreateNewRoundUpRequest request = new CreateNewRoundUpRequest();

    @Test
    void delegatesToRoundUpOrchestratorForWeekOf() {
      Instant weekOf = Instant.now();
      request.setWeekOf(weekOf.atOffset(ZoneOffset.UTC));
      when(orchestrator.performRoundUpForWeek(any(), any(), any(), any(), any()))
          .thenReturn(Optional.of(new Transfer(UUID.randomUUID())));

      controller.performRoundUp(request, VALID_AUTH);

      verify(orchestrator).weekOf(weekOf);
    }

    @Test
    void retrieveTransactionsForDates() {
      Instant start = Instant.now().plus(1, ChronoUnit.SECONDS);
      Instant end = Instant.now();
      WeekOf weekOf = new WeekOf(start, end);
      when(orchestrator.weekOf(any())).thenReturn(weekOf);
      when(orchestrator.performRoundUpForWeek(any(), any(), any(), any(), any()))
          .thenReturn(Optional.of(new Transfer(UUID.randomUUID())));

      UUID accountUid = UUID.randomUUID();
      UUID savingsGoalUid = UUID.randomUUID();
      UUID transferUid = UUID.randomUUID();
      request.setAccountUid(accountUid);
      request.setSavingsGoalUid(savingsGoalUid);
      request.setTransferUid(transferUid);
      request.setWeekOf(Instant.now().atOffset(ZoneOffset.UTC));

      controller.performRoundUp(request, VALID_AUTH);

      verify(orchestrator)
          .performRoundUpForWeek(weekOf, accountUid, savingsGoalUid, transferUid, "x.y.z");
    }

    @Test
    void returnsTransfer() {
      Transfer transfer = new Transfer(UUID.randomUUID());
      when(orchestrator.performRoundUpForWeek(any(), any(), any(), any(), any()))
          .thenReturn(Optional.of(transfer));
      request.setWeekOf(Instant.now().atOffset(ZoneOffset.UTC));

      CreateNewRoundUpResponse response = controller.performRoundUp(request, VALID_AUTH);

      assertThat(response.getTransferUid()).isEqualTo(transfer.transferUid());
    }

    @Test
    void throwsUnprocessableEntityIfNoTransferToMake() {
      request.setWeekOf(Instant.now().atOffset(ZoneOffset.UTC));

      assertThatThrownBy(() -> controller.performRoundUp(request, VALID_AUTH))
          .isInstanceOf(UnprocessableEntity.class);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"bearer x.y.z", "Bearer ", "Bearerx.y.z"})
    void throwsBadRequestExceptionWhenMalformedAuthorizationHeader(String authorizationHeader) {
      assertThatThrownBy(() -> controller.performRoundUp(request, authorizationHeader))
          .isInstanceOf(BadRequestException.class);
    }
  }
}
