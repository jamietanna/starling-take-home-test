package me.jvt.starling.interfaces.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.domain.services.SavingsGoalService;
import me.jvt.starling.interfaces.exceptions.BadRequestException;
import me.jvt.starling.interfaces.models.CreateNewSavingsGoalRequest;
import me.jvt.starling.interfaces.models.CreateNewSavingsGoalResponse;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SavingsGoalControllerTest {
  @Mock private SavingsGoalService service;

  @InjectMocks private SavingsGoalController controller;

  @Nested
  class Create {
    private final CreateNewSavingsGoalRequest request = new CreateNewSavingsGoalRequest();

    @Test
    void unpacksRequestAndDelegatesToService() {
      UUID accountUid = UUID.randomUUID();
      Account account = new Account(accountUid);
      CurrencyAndAmount target = new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(100));
      request.setName("Top-up");
      request.setAccountUid(accountUid);
      when(service.createRoundUpGoal(any(), any(), any(), any()))
          .thenReturn(new SavingsGoal(account, UUID.randomUUID(), "Savings Goal!"));

      controller.create(request, "Bearer x.y.z");

      verify(service).createRoundUpGoal(account, "Top-up", target, "x.y.z");
    }

    @Test
    void returnsFromService() {
      UUID accountUid = UUID.randomUUID();
      Account account = new Account(accountUid);
      UUID savingsGoalUid = UUID.randomUUID();
      when(service.createRoundUpGoal(any(), any(), any(), any()))
          .thenReturn(new SavingsGoal(account, savingsGoalUid, "Savings Goal!"));

      CreateNewSavingsGoalResponse response = controller.create(request, "Bearer x.y.z");

      assertThat(response.getSavingsGoalUid()).isEqualTo(savingsGoalUid);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"bearer x.y.z", "Bearer ", "Bearerx.y.z"})
    void throwsBadRequestExceptionWhenMalformedAuthorizationHeader(String authorizationHeader) {
      assertThatThrownBy(() -> controller.create(request, authorizationHeader))
          .isInstanceOf(BadRequestException.class);
    }
  }
}
