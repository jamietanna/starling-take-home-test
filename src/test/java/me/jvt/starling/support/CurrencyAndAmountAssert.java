package me.jvt.starling.support;

import me.jvt.starling.domain.models.Currency;
import me.jvt.starling.domain.models.CurrencyAndAmount;
import org.assertj.core.api.AbstractAssert;

public class CurrencyAndAmountAssert
    extends AbstractAssert<CurrencyAndAmountAssert, CurrencyAndAmount> {
  public CurrencyAndAmountAssert(CurrencyAndAmount currencyAndAmount) {
    super(currencyAndAmount, CurrencyAndAmountAssert.class);
  }

  public static CurrencyAndAmountAssert assertThat(CurrencyAndAmount currencyAndAmount) {
    return new CurrencyAndAmountAssert(currencyAndAmount);
  }

  public CurrencyAndAmountAssert hasMinorAmounts(long minorAmounts) {
    if (actual.amount().minorAmounts() != minorAmounts) {
      failWithMessage(
          "The Amount was expected to be <%s>, but was <%s>",
          minorAmounts, actual.amount().minorAmounts());
    }
    return this;
  }

  public CurrencyAndAmountAssert hasCurrency(Currency currency) {
    if (!actual.currency().equals(currency)) {
      failWithMessage(
          "The Currency was expected to be <%s>, but was <%s>", currency, actual.currency());
    }
    return this;
  }
}
