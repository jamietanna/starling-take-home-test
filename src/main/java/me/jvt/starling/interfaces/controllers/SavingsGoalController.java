package me.jvt.starling.interfaces.controllers;

import me.jvt.starling.domain.models.*;
import me.jvt.starling.domain.services.SavingsGoalService;
import me.jvt.starling.interfaces.exceptions.BadRequestException;
import me.jvt.starling.interfaces.models.CreateNewSavingsGoalRequest;
import me.jvt.starling.interfaces.models.CreateNewSavingsGoalResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/savings-goals")
public class SavingsGoalController {
  private final SavingsGoalService service;

  public SavingsGoalController(SavingsGoalService service) {
    this.service = service;
  }

  @PostMapping
  public CreateNewSavingsGoalResponse create(
      @RequestBody CreateNewSavingsGoalRequest request,
      @RequestHeader("authorization") String authorizationHeader) {
    String bearerToken = bearerToken(authorizationHeader);

    Account account = new Account(request.getAccountUid());
    CurrencyAndAmount target = new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(100));
    SavingsGoal created =
        service.createRoundUpGoal(account, request.getName(), target, bearerToken);

    CreateNewSavingsGoalResponse response = new CreateNewSavingsGoalResponse();
    response.setSavingsGoalUid(created.uid());

    return response;
  }

  private static String bearerToken(String authorizationHeader) {
    if (null == authorizationHeader || !authorizationHeader.startsWith("Bearer ")) {
      throw new BadRequestException();
    }

    String[] parts = authorizationHeader.split("Bearer ");
    if (parts.length != 2) {
      throw new BadRequestException();
    }

    if (parts[1] == null || parts[1].isEmpty()) {
      throw new BadRequestException();
    }

    return parts[1];
  }
}
