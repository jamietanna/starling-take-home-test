package me.jvt.starling.interfaces.controllers;

import java.util.Optional;
import me.jvt.starling.application.RoundUpOrchestrator;
import me.jvt.starling.application.models.WeekOf;
import me.jvt.starling.domain.models.Transfer;
import me.jvt.starling.interfaces.exceptions.BadRequestException;
import me.jvt.starling.interfaces.exceptions.UnprocessableEntity;
import me.jvt.starling.interfaces.models.CreateNewRoundUpRequest;
import me.jvt.starling.interfaces.models.CreateNewRoundUpResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/round-up")
public class RoundUpController {
  private final RoundUpOrchestrator service;

  public RoundUpController(RoundUpOrchestrator service) {
    this.service = service;
  }

  @PostMapping
  public CreateNewRoundUpResponse performRoundUp(
      @RequestBody CreateNewRoundUpRequest request,
      @RequestHeader("authorization") String authorizationHeader) {
    String bearerToken = bearerToken(authorizationHeader);

    WeekOf weekOf = service.weekOf(request.getWeekOf().toInstant());
    Optional<Transfer> maybeTransfer =
        service.performRoundUpForWeek(
            weekOf,
            request.getAccountUid(),
            request.getSavingsGoalUid(),
            request.getTransferUid(),
            bearerToken);

    Transfer transfer = maybeTransfer.orElseThrow(UnprocessableEntity::new);
    CreateNewRoundUpResponse response = new CreateNewRoundUpResponse();
    response.setTransferUid(transfer.transferUid());
    return response;
  }

  private static String bearerToken(String authorizationHeader) {
    if (null == authorizationHeader || !authorizationHeader.startsWith("Bearer ")) {
      throw new BadRequestException();
    }

    String[] parts = authorizationHeader.split("Bearer ");
    if (parts.length != 2) {
      throw new BadRequestException();
    }

    if (parts[1] == null || parts[1].isEmpty()) {
      throw new BadRequestException();
    }

    return parts[1];
  }
}
