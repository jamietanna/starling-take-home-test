package me.jvt.starling.interfaces.services;

import com.starlingbank.api.models.GetSettledTransactionsBetweenResponse;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.domain.services.TransactionsService;
import me.jvt.starling.interfaces.exceptions.InternalErrorException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class HttpTransactionsService implements TransactionsService {
  private final WebClient webClient;

  public HttpTransactionsService(WebClient webClient) {
    this.webClient = webClient;
  }

  @Override
  public Set<Transaction> findForPeriod(
      Account account, Instant start, Instant end, String bearerToken) {
    var entityMono =
        webClient
            .get()
            .uri(
                "/api/v2/feed/account/{accountUid}/settled-transactions-between?minTransactionTimestamp={minTransactionTimestamp}&maxTransactionTimestamp={maxTransactionTimestamp}",
                account.uid(),
                toUtc(start),
                toUtc(end))
            .accept(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + bearerToken)
            .retrieve()
            .onStatus(
                HttpStatus::is4xxClientError, ignored -> Mono.error(new InternalErrorException()))
            .onStatus(
                HttpStatus::is5xxServerError, ignored -> Mono.error(new InternalErrorException()))
            .toEntity(GetSettledTransactionsBetweenResponse.class);
    ResponseEntity<GetSettledTransactionsBetweenResponse> responseEntity = entityMono.block();

    // this has the chance of a NullPointerException would want to be handled
    var response = responseEntity.getBody();

    return response.getFeedItems().stream().map(toTransaction()).collect(Collectors.toSet());
  }

  private Function<GetSettledTransactionsBetweenResponse.SettledTransaction, Transaction>
      toTransaction() {
    return s -> {
      GetSettledTransactionsBetweenResponse.SettledTransaction.Amount settledAmount = s.getAmount();
      CurrencyAndAmount amount =
          new CurrencyAndAmount(
              Currency.from(settledAmount.getCurrency()), Amount.of(settledAmount.getMinorUnits()));

      return new Transaction(s.getFeedItemUid(), amount);
    };
  }

  private static String toUtc(Instant instant) {
    return DateTimeFormatter.ISO_INSTANT.format(instant);
  }
}
