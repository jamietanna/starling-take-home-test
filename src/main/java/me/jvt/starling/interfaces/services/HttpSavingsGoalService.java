package me.jvt.starling.interfaces.services;

import com.starlingbank.api.models.CreateSavingsGoalRequest;
import com.starlingbank.api.models.CreateSavingsGoalResponse;
import com.starlingbank.api.models.TransferToSavingsGoalRequest;
import com.starlingbank.api.models.TransferToSavingsGoalResponse;
import java.util.UUID;
import me.jvt.starling.domain.models.Account;
import me.jvt.starling.domain.models.CurrencyAndAmount;
import me.jvt.starling.domain.models.SavingsGoal;
import me.jvt.starling.domain.models.Transfer;
import me.jvt.starling.domain.services.SavingsGoalService;
import me.jvt.starling.interfaces.exceptions.InternalErrorException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class HttpSavingsGoalService implements SavingsGoalService {
  private final WebClient webClient;

  public HttpSavingsGoalService(WebClient webClient) {
    this.webClient = webClient;
  }

  @Override
  public SavingsGoal createRoundUpGoal(
      Account account, String name, CurrencyAndAmount target, String bearerToken) {
    CreateSavingsGoalRequest request = constructSavingsGoalRequest(name, target);

    Mono<ResponseEntity<CreateSavingsGoalResponse>> responseEntityMono =
        webClient
            .put()
            .uri("/api/v2/account/{accountUid}/savings-goals", account.uid())
            .contentType(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + bearerToken)
            .bodyValue(request)
            .retrieve()
            .onStatus(
                HttpStatus::is4xxClientError, ignored -> Mono.error(new InternalErrorException()))
            .onStatus(
                HttpStatus::is5xxServerError, ignored -> Mono.error(new InternalErrorException()))
            .toEntity(CreateSavingsGoalResponse.class);
    ResponseEntity<CreateSavingsGoalResponse> responseEntity = responseEntityMono.block();

    // this has the chance of a NullPointerException would want to be handled
    CreateSavingsGoalResponse response = responseEntity.getBody();

    // this has the chance of a NullPointerException would want to be handled
    return new SavingsGoal(account, response.getSavingsGoalUid(), name);
  }

  @Override
  public Transfer transferRoundUp(
      SavingsGoal savingsGoal, CurrencyAndAmount amount, Transfer transfer, String bearerToken) {
    TransferToSavingsGoalRequest request = constructTransferRequest(amount);

    Mono<ResponseEntity<TransferToSavingsGoalResponse>> responseEntityMono =
        webClient
            .put()
            .uri(
                "/api/v2/account/{accountUid}/savings-goals/{savingsGoalUid}/add-money/{transferUid}",
                savingsGoal.account().uid(),
                savingsGoal.uid(),
                transfer.transferUid())
            .contentType(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + bearerToken)
            .bodyValue(request)
            .retrieve()
            .onStatus(
                HttpStatus::is4xxClientError, ignored -> Mono.error(new InternalErrorException()))
            .onStatus(
                HttpStatus::is5xxServerError, ignored -> Mono.error(new InternalErrorException()))
            .toEntity(TransferToSavingsGoalResponse.class);
    ResponseEntity<TransferToSavingsGoalResponse> responseEntity = responseEntityMono.block();

    // this has the chance of a NullPointerException would want to be handled
    TransferToSavingsGoalResponse response = responseEntity.getBody();

    // this has the chance of a NullPointerException would want to be handled
    return new Transfer(UUID.fromString(response.getTransferUid()));
  }

  private CreateSavingsGoalRequest constructSavingsGoalRequest(
      String name, CurrencyAndAmount target) {
    CreateSavingsGoalRequest request = new CreateSavingsGoalRequest();
    request.setCurrency(target.currency().getIsoName());
    request.setName(name);
    CreateSavingsGoalRequest.Target requestTarget = new CreateSavingsGoalRequest.Target();
    requestTarget.setCurrency(target.currency().getIsoName());
    requestTarget.setMinorUnits(target.amount().minorAmounts());
    request.setTarget(requestTarget);
    return request;
  }

  private static TransferToSavingsGoalRequest constructTransferRequest(CurrencyAndAmount amount) {
    TransferToSavingsGoalRequest request = new TransferToSavingsGoalRequest();
    TransferToSavingsGoalRequest.Amount requestAmount = new TransferToSavingsGoalRequest.Amount();
    requestAmount.setCurrency(amount.currency().getIsoName());
    requestAmount.setMinorUnits(amount.amount().minorAmounts());
    request.setAmount(requestAmount);

    return request;
  }
}
