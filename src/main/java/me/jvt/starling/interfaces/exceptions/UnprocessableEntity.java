package me.jvt.starling.interfaces.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UnprocessableEntity extends ResponseStatusException {
  public UnprocessableEntity() {
    super(HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
