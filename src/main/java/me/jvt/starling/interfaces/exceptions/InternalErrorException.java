package me.jvt.starling.interfaces.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InternalErrorException extends ResponseStatusException {
  public InternalErrorException() {
    super(HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
