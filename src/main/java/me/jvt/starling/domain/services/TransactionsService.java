package me.jvt.starling.domain.services;

import java.time.Instant;
import java.util.Set;
import me.jvt.starling.domain.models.Account;
import me.jvt.starling.domain.models.Transaction;

public interface TransactionsService {
  Set<Transaction> findForPeriod(Account account, Instant start, Instant end, String bearerToken);
}
