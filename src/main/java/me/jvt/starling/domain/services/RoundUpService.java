package me.jvt.starling.domain.services;

import java.util.Set;
import java.util.stream.Collectors;
import me.jvt.starling.domain.models.*;
import org.springframework.stereotype.Service;

@Service
public class RoundUpService {
  /**
   * Perform a "round up" for a given {@link Transaction}.
   *
   * <p>Only supports <code>me.jvt.starling.domain.models.Currency.POUND_STERLING</code> at this
   * time.
   *
   * @return the {@link RoundUp} containing the rounded amount
   * @throws CurrencyNotSupportedException if the currency provided is not supported
   */
  public RoundUp roundUp(Transaction transaction) {
    // This is required because we may need to handle other currencies differently
    if (!transaction.currencyAndAmount().currency().equals(Currency.POUND_STERLING)) {
      throw new CurrencyNotSupportedException();
    }

    long rounded = roundToOneHundred(transaction.currencyAndAmount().amount().minorAmounts());

    return new RoundUp(
        transaction,
        new CurrencyAndAmount(transaction.currencyAndAmount().currency(), Amount.of(rounded)));
  }

  /**
   * Calculate the total sum of many {@link RoundUp}s.
   *
   * <p>Only supports <code>me.jvt.starling.domain.models.Currency.POUND_STERLING</code> at this
   * time.
   *
   * @return the {@link CurrencyAndAmount} containing the total amount
   * @throws CurrencyNotSupportedException if the currency provided is not supported
   */
  public CurrencyAndAmount calculateTotalRoundUp(Set<RoundUp> roundUps) {
    var filtered =
        roundUps.stream()
            .filter(r -> r.roundUp().currency().equals(Currency.POUND_STERLING))
            .collect(Collectors.toSet());
    // This is required because we may need to handle other currencies differently
    if (filtered.size() != roundUps.size()) {
      throw new CurrencyNotSupportedException();
    }
    long total = filtered.stream().mapToLong(r -> r.roundUp().amount().minorAmounts()).sum();

    return new CurrencyAndAmount(Currency.POUND_STERLING, Amount.of(total));
  }

  static class CurrencyNotSupportedException extends RuntimeException {}

  private long roundToOneHundred(long minorAmounts) {
    long remainder = minorAmounts % 100;
    long rounded = 100 - remainder;
    if (rounded == 100) {
      return 0;
    }

    return rounded;
  }
}
