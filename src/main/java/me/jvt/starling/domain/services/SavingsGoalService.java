package me.jvt.starling.domain.services;

import me.jvt.starling.domain.models.Account;
import me.jvt.starling.domain.models.CurrencyAndAmount;
import me.jvt.starling.domain.models.SavingsGoal;
import me.jvt.starling.domain.models.Transfer;

public interface SavingsGoalService {

  /**
   * Create a {@link SavingsGoal} for a given {@link Account}.
   *
   * @param account the {@link Account} for which to create the {@link SavingsGoal} for
   * @param name the name of the {@link SavingsGoal} to create
   * @param target the target amount for the Savings Goal
   * @param bearerToken the bearer token tied to the {@link Account}'s owner
   * @return the created {@link SavingsGoal}
   */
  SavingsGoal createRoundUpGoal(
      Account account, String name, CurrencyAndAmount target, String bearerToken);

  /**
   * Transfer a {@link CurrencyAndAmount} to a given {@link SavingsGoal}.
   *
   * @param savingsGoal the {@link SavingsGoal} to transfer to
   * @param amount the {@link CurrencyAndAmount} that should be transferred
   * @param transfer the {@link Transfer} object, for idempotence
   * @param bearerToken the bearer token tied to the {@link SavingsGoal}'s owner
   * @return <code>transfer</code> that was provided
   */
  Transfer transferRoundUp(
      SavingsGoal savingsGoal, CurrencyAndAmount amount, Transfer transfer, String bearerToken);
}
