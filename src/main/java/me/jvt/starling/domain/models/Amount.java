package me.jvt.starling.domain.models;

public record Amount(long minorAmounts) {

  public static Amount of(long minorAmounts) {
    return new Amount(minorAmounts);
  }
}
