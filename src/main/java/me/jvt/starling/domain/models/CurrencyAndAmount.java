package me.jvt.starling.domain.models;

public record CurrencyAndAmount(Currency currency, Amount amount) {}
