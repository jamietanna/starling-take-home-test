package me.jvt.starling.domain.models;

import java.util.UUID;

/** An Account as defined by <code>/api/v2/accounts</code>. */
public record Account(UUID uid) {}
