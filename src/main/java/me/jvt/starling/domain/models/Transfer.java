package me.jvt.starling.domain.models;

import java.util.UUID;

public record Transfer(UUID transferUid) {}
