package me.jvt.starling.domain.models;

public record Transaction(String uid, CurrencyAndAmount currencyAndAmount) {}
