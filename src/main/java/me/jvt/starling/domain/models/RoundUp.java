package me.jvt.starling.domain.models;

public record RoundUp(Transaction transaction, CurrencyAndAmount roundUp) {}
