package me.jvt.starling.domain.models;

import java.util.UUID;

public record SavingsGoal(Account account, UUID uid, String name) {}
