package me.jvt.starling.domain.models;

public enum Currency {
  POUND_STERLING("GBP"),
  UNITED_STATES_DOLLARS("USD");

  private final String isoName;

  Currency(String isoName) {

    this.isoName = isoName;
  }

  public static Currency from(String currencyString) {
    for (Currency currency : values()) {
      if (currency.getIsoName().equals(currencyString)) {
        return currency;
      }
    }

    throw new IllegalArgumentException(
        "Could not find a valid Currency from the provided value `" + currencyString + "`");
  }

  public String getIsoName() {
    return isoName;
  }
}
