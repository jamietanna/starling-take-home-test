package me.jvt.starling.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfiguration {
  @Bean
  public WebClient webClient(
      WebClient.Builder webClientBuilder,
      @Value("https://api-sandbox.starlingbank.com") String apiBaseUrl) {
    return webClientBuilder.baseUrl(apiBaseUrl).build();
  }
}
