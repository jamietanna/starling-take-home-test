package me.jvt.starling.application.repositories;

import java.util.Optional;
import me.jvt.starling.domain.models.RoundUp;

public interface RoundUpRepository {
  Optional<RoundUp> findById(String id);

  void save(RoundUp roundUp);
}
