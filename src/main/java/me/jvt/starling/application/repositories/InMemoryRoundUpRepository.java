package me.jvt.starling.application.repositories;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import me.jvt.starling.domain.models.RoundUp;
import org.springframework.stereotype.Repository;

/*
 * Note that this class has not been unit tested, as it's "trivial enough".
 */
@Repository
public class InMemoryRoundUpRepository implements RoundUpRepository {

  private final Map<String, RoundUp> items = new HashMap<>();

  @Override
  public Optional<RoundUp> findById(String id) {
    return Optional.ofNullable(items.get(id));
  }

  @Override
  public void save(RoundUp roundUp) {
    items.put(roundUp.transaction().uid(), roundUp);
  }
}
