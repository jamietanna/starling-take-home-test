package me.jvt.starling.application.models;

import java.time.Instant;

public record WeekOf(Instant start, Instant end) {}
