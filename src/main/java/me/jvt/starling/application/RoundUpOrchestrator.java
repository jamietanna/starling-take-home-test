package me.jvt.starling.application;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import me.jvt.starling.application.models.WeekOf;
import me.jvt.starling.application.repositories.RoundUpRepository;
import me.jvt.starling.domain.models.*;
import me.jvt.starling.domain.services.RoundUpService;
import me.jvt.starling.domain.services.SavingsGoalService;
import me.jvt.starling.domain.services.TransactionsService;
import org.springframework.stereotype.Component;

/**
 * An application-layer service, used to perform the business logic to perform round-ups across
 * multiple {@link me.jvt.starling.domain.models.Transaction}s.
 */
@Component
public class RoundUpOrchestrator {

  private final TransactionsService transactionsService;
  private final RoundUpRepository roundUpRepository;
  private final RoundUpService roundUpService;
  private final SavingsGoalService savingsGoalService;

  public RoundUpOrchestrator(
      TransactionsService transactionsService,
      RoundUpRepository roundUpRepository,
      RoundUpService roundUpService,
      SavingsGoalService savingsGoalService) {
    this.transactionsService = transactionsService;
    this.roundUpRepository = roundUpRepository;
    this.roundUpService = roundUpService;
    this.savingsGoalService = savingsGoalService;
  }

  /**
   * Determine the {@link WeekOf} week boundary for a given point in time.
   *
   * @param now a given point in time
   * @return the {@link WeekOf} with the <code>start</code> as midnight on Monday, and <code>end
   *     </code> 23:59 on Sunday
   */
  public WeekOf weekOf(Instant now) {
    Instant start =
        now.atZone(ZoneId.of("UTC"))
            .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
            .truncatedTo(ChronoUnit.DAYS)
            .toInstant();
    Instant end =
        now.atZone(ZoneId.of("UTC"))
            .with(TemporalAdjusters.next(DayOfWeek.MONDAY))
            .truncatedTo(ChronoUnit.DAYS)
            .minus(1, ChronoUnit.SECONDS)
            .toInstant();

    return new WeekOf(start, end);
  }

  public Optional<Transfer> performRoundUpForWeek(
      WeekOf weekOf, UUID accountUid, UUID savingsGoalUid, UUID transferUid, String bearerToken) {
    Account account = new Account(accountUid);
    Set<Transaction> transactions =
        transactionsService.findForPeriod(account, weekOf.start(), weekOf.end(), bearerToken);
    Set<RoundUp> rounded =
        transactions.stream()
            .filter(t -> roundUpRepository.findById(t.uid()).isEmpty())
            .map(roundUpService::roundUp)
            .collect(Collectors.toSet());
    CurrencyAndAmount toTransfer = roundUpService.calculateTotalRoundUp(rounded);
    if (toTransfer.amount().minorAmounts() == 0) {
      return Optional.empty();
    }
    // LOG
    // LOCK
    Transfer transfer =
        savingsGoalService.transferRoundUp(
            createSavingsGoal(account, savingsGoalUid),
            toTransfer,
            new Transfer(transferUid),
            bearerToken);
    for (var roundUp : rounded) {
      roundUpRepository.save(roundUp);
    }

    return Optional.of(transfer);
    // </LOCK>
  }

  private static SavingsGoal createSavingsGoal(Account account, UUID savingsGoalUid) {
    // we could alternatively look up the name, but it's not necessary. Maybe a better choice here
    // would be to make this an `Optional` to make it clear it's possibly not present
    return new SavingsGoal(account, savingsGoalUid, null);
  }
}
