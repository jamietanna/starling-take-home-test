package com.starlingbank.api.models;

public class TransferToSavingsGoalResponse {
  private String transferUid;

  public String getTransferUid() {
    return transferUid;
  }

  public void setTransferUid(String transferUid) {
    this.transferUid = transferUid;
  }
}
