package com.starlingbank.api.models;

public class TransferToSavingsGoalRequest {
  private Amount amount;

  public Amount getAmount() {
    return amount;
  }

  public void setAmount(Amount amount) {
    this.amount = amount;
  }

  public static class Amount {
    private String currency;
    private long minorUnits;

    public String getCurrency() {
      return currency;
    }

    public void setCurrency(String currency) {
      this.currency = currency;
    }

    public long getMinorUnits() {
      return minorUnits;
    }

    public void setMinorUnits(long minorUnits) {
      this.minorUnits = minorUnits;
    }
  }
}
