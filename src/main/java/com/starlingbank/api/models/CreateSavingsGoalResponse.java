package com.starlingbank.api.models;

import java.util.UUID;

public class CreateSavingsGoalResponse {
  private UUID savingsGoalUid;

  public UUID getSavingsGoalUid() {
    return savingsGoalUid;
  }

  public void setSavingsGoalUid(UUID savingsGoalUid) {
    this.savingsGoalUid = savingsGoalUid;
  }
}
