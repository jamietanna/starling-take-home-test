package com.starlingbank.api.models;

public class CreateSavingsGoalRequest {
  private String name;
  private String currency;
  private Target target;

  public static class Target {
    private String currency;
    private long minorUnits;

    public String getCurrency() {
      return currency;
    }

    public void setCurrency(String currency) {
      this.currency = currency;
    }

    public long getMinorUnits() {
      return minorUnits;
    }

    public void setMinorUnits(long minorUnits) {
      this.minorUnits = minorUnits;
    }
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Target getTarget() {
    return target;
  }

  public void setTarget(Target target) {
    this.target = target;
  }
}
