package com.starlingbank.api.models;

import java.util.List;

public class GetSettledTransactionsBetweenResponse {
  private List<SettledTransaction> feedItems;

  public List<SettledTransaction> getFeedItems() {
    return feedItems;
  }

  public void setFeedItems(List<SettledTransaction> feedItems) {
    this.feedItems = feedItems;
  }

  public static class SettledTransaction {
    private String feedItemUid;
    private Amount amount;

    public String getFeedItemUid() {
      return feedItemUid;
    }

    public void setFeedItemUid(String feedItemUid) {
      this.feedItemUid = feedItemUid;
    }

    public Amount getAmount() {
      return amount;
    }

    public void setAmount(Amount amount) {
      this.amount = amount;
    }

    public static class Amount {
      private String currency;
      private long minorUnits;

      public String getCurrency() {
        return currency;
      }

      public void setCurrency(String currency) {
        this.currency = currency;
      }

      public long getMinorUnits() {
        return minorUnits;
      }

      public void setMinorUnits(long minorUnits) {
        this.minorUnits = minorUnits;
      }
    }
  }
}
